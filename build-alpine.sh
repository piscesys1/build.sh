#!/bin/bash

echo '    Welcome to PiscesDE build script! This script works on alpine-based distros and aims at making PiscesDE usable on other Linux distros.'
echo '    This script can build and install one component / all of the components of PiscesDE automatically based on your choice.'
# shellcheck disable=SC2016
echo '    By default, all of the git repositories will be cloned to ~/piscesys from https://gitlab.com/piscesys . You can change them by simply modifing $REPO_PATH and $GIT_REPO_URL.'
echo '    In case of errors, you can report them to https://gitlab.com/piscesys/Build.sh/issues .'
echo '------'
echo 'Authors: moyu:     Original author'
echo '         wrefiyu:       Co-author'
echo '------'
echo 'You should read all the above before starting to build.'

echo "The next step will install necessary dependencies for building."




  printf "正在clone源码"

cd || exit
sudo mkdir piscesys
cd piscesys || exit
sudo git clone https://gitlab.com/piscesys/fishui
sudo git clone https://gitlab.com/piscesys/settings
sudo git clone https://gitlab.com/piscesys/core
sudo git clone https://gitlab.com/piscesys/icons
sudo git clone https://gitlab.com/piscesys/wallpapers
sudo git clone https://gitlab.com/piscesys/qt-plugins
sudo git clone https://gitlab.com/piscesys/libpisces
sudo git clone https://gitlab.com/piscesys/terminal
sudo git clone https://gitlab.com/piscesys/screenshot
sudo git clone https://gitlab.com/piscesys/screenlocker
sudo git clone https://gitlab.com/piscesys/kwin-plugins
sudo git clone https://gitlab.com/piscesys/filemanager
sudo git clone https://gitlab.com/piscesys/launcher
sudo git clone https://gitlab.com/piscesys/statusbar
sudo git clone https://gitlab.com/piscesys/dock
sudo git clone https://gitlab.com/piscesys/calculator
sudo git clone https://gitlab.com/piscesys/sddm-theme
sudo git clone https://gitlab.com/piscesys/texteditor
sudo git clone https://gitlab.com/piscesys/appmotor
sudo git clone https://gitlab.com/piscesys/gtk-themes
sudo git clone https://gitlab.com/piscesys/plymouth-theme
sudo git clone https://gitlab.com/piscesys/videoplayer
sudo git clone https://gitlab.com/piscesys/grub-theme
sudo git clone https://gitlab.com/piscesys/cursor-themes
sudo git clone https://gitlab.com/piscesys/calamares

  printf "正在安装依赖"

sudo apk add extra-cmake-modules pkgconf pulseaudio-qt-dev kwindowsystem polkit pulseaudio-dev pulseaudio qt5-qtbase-x11 qt5-qtquickcontrols2 qt5-qtx11extras qt5-qttools polkit-qt-1 g++ make qt5-qtbase-dev qt5-qttools-dev qt5-qtbase qt5-qtsystems-dev qt5-qtquickcontrols2-dev qt5-qtquickcontrols qt5-qtsvg-dev qt5-qtscript-dev qt5-qtx11extras-dev kfilemetadata-dev kwin-dev breeze-dev libx11-dev python3 syntax-highlighting-dev yaml-cpp pulseaudio-bluez bluez-qt bluez-dev kde-dev-scripts kwindowsystem-dev kwin-lang kwindowsystem-lang bluez-firmware xdg-desktop-portal-kde libxdg-basedir-dev qtxdg-tools-dev libqtxdg-dev icu-dev qt5-qtwebglplugin qt5-qtspeech-dev qt5-qtlocation-dev libarchive-qt qt5-qtimageformats xorg-server-dev xf86-input-synaptics-dev xf86-input-libinput-dev nemo-qml-plugin-dbus dbus-cpp qt5-qdbusviewer libdbusmenu-qt-dev kwin-dbg kross bluez-libs bluez-plugins bluez-qt-dev qt5-qtsensors-dev networkmanager-qt modemmanager-qt-dev networkmanager-qt-dev kscreen libkscreen-dev libcanberra-dev kio-dev qt5-qtserialbus dmenu yaml-cpp-dev mpv-dev qt5-qtwebglplugin-dev qt5-qtwebengine-dev wimlib xf86-input-libinput-dev alpine-conf xorgxrdp-dev xf86-input-synaptics-dev xorg-server-common mesa-dri-gallium py3-opengl gst-plugins-good-qt systemsettings glfw


  printf "正在编译"


pdir=$(pwd)
subs=$(ls "$pdir")
for i in "${subs[@]}"; do 
        if [ -d "$i" ]; then 
                printf "========================== update %s ============================ \n" "$i"
                cd "$i" || exit
                rm -rf build/
                git pull
                mkdir build
                cd build || exit
                cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ..
                make
                sudo make install
                printf "========================== done %s ==============================\n" "$i"
                cd ../../ || exit
        fi;
done

