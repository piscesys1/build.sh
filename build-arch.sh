#!/bin/bash
echo '    Welcome to PiscesDE build script! This script works on most Arch/Manjaro-based distros and aims at making PiscesDE usable on other Linux distros.'
echo '    This script can build and install one component / all of the components of PiscesDE automatically based on your choice.'
echo '    By default, all of the git repositories will be cloned to ~/piscesys from https://gitlab.com/piscesys . You can change them by simply modifing $REPO_PATH and $GIT_REPO_URL.'
echo '    In case of errors, you can report them to https://gitlab.com/piscesys/Build.sh/issues .'
echo '------'
echo 'Authors: moyu:      Original author'
echo '         wrefiyu:   Co-author'
echo '         Fuyuki S.: Co-author'
echo '------'
echo 'You should read all the above before starting to build.'

echo "The next step will install necessary dependencies for building."
read -r -p "Continue? [Y/n] " input
case $input in
    [yY][eE][sS]|[yY])
        echo "------"
		;;
    *)
		echo "Quitting."
		exit 1
		;;
esac

# Set repository path & URL here.
# Please mind that NO SPLASH ("/") should appear at the end of the path.
REPO_PATH=~/piscesys
GIT_REPO_URL=https://gitlab.com/piscesys

echo 'Installing dependencies:'
sudo pacman -S extra-cmake-modules pkgconf qt5-base qt5-quickcontrols2 qt5-x11extras qt5-tools kwindowsystem polkit polkit-qt5 pulseaudio
# TODO: Check dependencies
echo 'Dependencies installed.'
echo '------'

#function standardBuild_pacman(){} # TODO

function standardBuild_make(){
    echo "$1 build start:"
        cd "$REPO_PATH"
        echo "Cloning:"
        git clone "$GIT_REPO_URL/$1.git"
        cd $1
#         echo "Installing the dependencies:"
#         sudo mk-build-deps ./debian/control -i -t "apt-get --yes" -r
        echo "Building:"
        mkdir build
        cd build
        cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ..
        make
    echo "$1 built."

    echo "$1 installation start:"
        sudo make install
    echo "$1 installed."
    echo '------'
}

WAY_TO_INSTALL="make" # TODO: Add pacman packaging method

function Build(){
    case $WAY_TO_INSTALL in
#         pacman)
#             standardBuild_pacman $1
#         ;;
        make)
            standardBuild_make $1
        ;;
        *)
            echo "Unknown ERROR"
            exit 1
        ;;
    esac
}

# Package list here. Once new packages were added please add the repository name here.
PAC_LIST="fishui libpisces qt-plugins kwin-plugins core daemon filemanager dock screenshot terminal launcher settings debinstaller icons gtk-themes statusbar updator screenlocker calculator videoplayer sddm-theme appmotor wallpapers calamares texteditor cursor-themes grub-theme plymouth-theme"

function Selection()
{
    echo 'Input the number of the component you want to build and press enter (1 for all, 2 to quit)'
    PS3='Input a number (ENTER to see the list):'
    select i in ALL QUIT $PAC_LIST
    do
        if test "$i" == "ALL" ;
        then
            for j in $PAC_LIST
            do
                Build $j
            done
        elif test "$i" == "QUIT" ;
        then
            echo "Quitting."
            exit 0
        elif test "$i" != "" ;
        then
            Build $i
        else
            echo 'Invalid number. Please check your input.'
        fi
    done
}

echo 'In which way would you like to install?'
select QUE in "make install - Install the binaries directly" #"pacman - Build Arch Linux packages and install with pacman (Easy to remove)"
do
    if test "$QUE" == "pacman - Build Arch Linux packages and install with pacman (Easy to remove)"
    then
        WAY_TO_INSTALL="pacman"
        echo '------'
        Selection
    elif test "$QUE" == "make install - Install the binaries directly"
        then
            WAY_TO_INSTALL="make"
            echo '------'
            Selection
        fi
    fi
done
